import React, { Component } from 'react';
import App from './App';
import './App.css';

class Home extends Component {
  // calls the login method in authentication service
  login = () => {
    this.props.auth.login();
  }
  logout = () => {
		this.props.auth.logout();
	  }
  render() {
    // calls the isAuthenticated method in authentication service
    const { isAuthenticated } = this.props.auth;
    return (
      <div>
        {
          isAuthenticated() &&
          <div>
            <App logout={this.logout} selectedPage={'Rooms'}/>
          </div>
        }
        {
          !isAuthenticated() && (
            <div className='login-screen-main'>
              <div className='login-screen-header'>
                <div className='header'>Welcome to the Quarantine Karaoke Cafe!</div>
                <div className='header'>Entrance is free - Please login first</div>
              </div>
              <div className='login-screen-body'>
                <div className='login-link' onClick={this.login}>
                  <a onClick={this.login}>Log In</a>
                </div>
              </div>
            </div>
          )
        }
      </div>
      );
    }
  }

  export default Home;