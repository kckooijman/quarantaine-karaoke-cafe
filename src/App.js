import React, { Component } from "react";
import './App.css';

import HomeBody from "./components/HomeBody";

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {selectedPage: this.props.selectedPage};  
	}

	setAboutQKC = () => {
		this.setState({selectedPage: 'AboutQKC'});
		console.log(this.selectedPage);
	}
	setAboutCoronavirus = () => {
		this.setState({selectedPage: 'AboutCoronavirus'});
		console.log(this.selectedPage);
	}
	setRooms = () => {
		this.setState({selectedPage: 'Rooms'});
		console.log(this.selectedPage);
	}

	render() {
		console.log(this.selectedPage);
		return (
		<div id='main-grid' className='main-grid'>
			<div id='main-grid-header' className='main-grid-header'>
				<div className='header-button' onClick={this.setAboutQKC}>
					<a onClick={this.setAboutQKC}>About QKC</a>
				</div>
				<div className='header-button' onClick={this.setAboutCoronavirus}>
					<a onClick={this.setAboutCoronavirus}>About Coronavirus</a>
				</div>
				<div className='header-button' onClick={this.setRooms}>
					<a onClick={this.setRooms}>Rooms</a>
				</div>
				<div className='header-button' onClick={this.props.logout}>
					<a onClick={this.props.logout}>Log Out</a>
				</div>
			</div>
			<div id='main-grid-body' className='main-grid-body'>
				<div id='main-elements-holder' className='main-elements-holder'>
				<HomeBody selectedPage={this.state.selectedPage}/>
				</div>
			</div>
			
		</div>
		);
    }
}

export default App;
