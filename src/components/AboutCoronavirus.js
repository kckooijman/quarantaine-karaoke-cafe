import React, { Component } from "react";
import '../App.css';

class AboutCoronavirus extends Component {

  render() {
    return (
      <div className='about-coronavirus'>
        <h2>Important Note</h2>
        <p> Please know that we are no experts on this subject. Below you can find some general information about the current Corona virus (Covid-19).
            Please refer to your national/governmental health institution for more specific information.
        </p>
        <h2>The Corona Virus</h2>
        <p> Coronaviruses (CoV) are a large family of viruses that cause illness ranging from the common cold to more severe diseases such as Middle East Respiratory Syndrome (MERS-CoV) and Severe Acute Respiratory Syndrome (SARS-CoV).
            Coronavirus disease 2019 (COVID-19) is a respiratory illness that can spread from person to person. The virus that causes COVID-19 is a new coronavirus that was first identified during an investigation into an outbreak in Wuhan, China.
        </p>
        <p> Since its discovery, the virus has spread quickly throughout the world affecting almost every country within a few months.
        </p>
        <h2>Can I get the virus?</h2>
        <p> Yes. COVID-19 is spreading from person to person in parts of the world. Risk of infection from the virus that causes COVID-19 is higher for people who are close contacts of someone known to have COVID-19.
        </p>
        <h2>How does the virus spread?</h2>
        <p> The virus is thought to spread mainly between people who are in close contact with one another through respiratory droplets produced when an infected person coughs or sneezes. 
            It also may be possible that a person can get COVID-19 by touching a surface or object that has the virus on it and then touching their own mouth, nose, or possibly their eyes.
        </p>
        <h2>What are the symptoms?</h2>
        <p> Patients with COVID-19 have had mild to severe symptoms of:
        </p>
        <ul>
          <li>
            Fever
          </li>
          <li>
            Cough
          </li>
          <li>
            Shortness of breath
          </li>
        </ul>
        <h2>What can I do?</h2>
        <p> First of all; refer to your national health institution for the best and latest tips. Some general tips are:
        </p>
        <ul>
          <li>
            Wash your hands regularly
          </li>
          <li>
            Limit physical contact
          </li>
          <li>
            Limit the time you spend outside. Only go out when you have to buy food or need to visit the doctor
          </li>
          <li>
            Try to avoid elderly people. They are more susceptible to the virus and can develop more serious (sometimes even fatal) symptoms
          </li>
        </ul>
        <h2>Where can I find more information?</h2>
        <p> You can get more information from you national/governmental health institution or national news. Below you can find links to some of these governmental institutions per country. You can also refer to the website of the <a href='https://www.who.int/' className='link-to-website'>World Health Organization here</a>.
        </p>
      </div>
    );
  }
}

export default AboutCoronavirus;
