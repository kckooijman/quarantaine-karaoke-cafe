import React, { Component } from "react";
import '../App.css';
import VideoMainComponent from "./VideoMainComponent";
import AboutCoronavirus from "./AboutCoronavirus";
import AboutQKC from "./AboutQKC";

class HomeBody extends Component {

    render() {

        if(this.props.selectedPage==='AboutQKC'){
            return (
                <div id='AboutQKCPage'>
                    <div id='About-QKC' className='about-page'>
                        <AboutQKC />
                    </div>
                </div>
            );
        }

        if(this.props.selectedPage==='AboutCoronavirus'){
            return (
                <div id='AboutCoronavirusPage'>
                    <div id='About-Coronavirus' className='about-page'>
                        <AboutCoronavirus />
                    </div>
                </div>
            );
        }

        if(this.props.selectedPage==='Rooms'){
            return (
                <div id='KaraokePage' className='karaoke-page'>
                    <div id='youtube-viewer' className='youtube-viewer'>
                        <VideoMainComponent />
                    </div>
                    <div id='chat-room' className='chat-room'>
                        &nbsp;
                    </div>
                </div>
            );
        }

        
    }
}

export default HomeBody;
